﻿using System;

namespace EgnParser.Models
{
    public class Citizen
    {
        public string EGN { get; set; }

        public Region Region { get; set; }

        public Gender Gender { get; set; }

        public DateTime Birthdate { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    }

    public enum Region
    {
        Blagoevgrad = 43,
        Burgas = 93,
        Varna = 139,
        VelikoTarnovo = 169,
        Vidin = 183,
        Vraca = 217,
        Gabrovo = 233,
        Kardzhali = 281,
        Kyustendil = 301,
        Lovech = 319,
        Montana = 341,
        Pazardzhik = 377,
        Pernik = 395,
        Pleven = 435,
        Plovdiv = 501,
        Razgrad = 527,
        Ruse = 555,
        Silistra = 575,
        Sliven = 601,
        Smolyan = 623,
        SofiaCity = 721,
        SofiaDistrict = 751,
        StaraZagora = 789,
        Dobrich = 821,
        Targovishte = 843,
        Haskovo = 871,
        Shumen = 903,
        Yambol = 925,
        Neizvesten = 999
    }
}
