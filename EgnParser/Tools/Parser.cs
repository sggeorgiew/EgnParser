﻿using EgnParser.Models;
using System;

namespace EgnParser.Tools
{
    internal class Parser
    {
        private static readonly int[] EGN_WEIGHTS = { 2, 4, 8, 5, 10, 9, 7, 3, 6 };
        private static string _egn;

        public static bool Validate(string egn)
        {
            if (egn.Length != 10)
                return false;

            _egn = egn;

            try
            {
                DateTime date = GetBirthdate();
                if (date == new DateTime())
                    return false;

                int checkSum = int.Parse(egn.Substring(9, 1));
                int egnSum = 0;

                for (int i = 0; i < 9; i++)
                    egnSum += int.Parse(egn.Substring(i, 1)) * EGN_WEIGHTS[i];

                int validCheckSum = egnSum % 11;
                if (validCheckSum == 10)
                    validCheckSum = 0;

                if (checkSum == validCheckSum)
                    return true;
                else
                    return false;
            }
            catch (FormatException fe)
            {
                throw new FormatException("Invalid input!", fe);
            }
        }

        public static Citizen GetCitizen(string egn)
        {
            if (!Validate(egn))
                return null;

            Citizen citizen = new Citizen()
            {
                Birthdate = GetBirthdate(),
                Region = GetRegion(),
                Gender = GetGender(),
                EGN = _egn
            };

            return citizen;
        }

        private static DateTime GetBirthdate()
        {
            short year = GetYear();
            byte month = GetMonth();
            byte day = GetDay();

            if (month > 40)
            {
                month -= 40;
                year += 2000;
            }
            else if (month > 20)
            {
                month -= 20;
                year += 1800;
            }
            else
            {
                year += 1900;
            }

            DateTime.TryParse($"{day}/{month}/{year}", out DateTime temp);

            return temp;
        }

        private static Region GetRegion()
        {
            short regionNumber = GetRegionNumber();
            short firstRegionNumber = 0;

            foreach (Region region in Enum.GetValues(typeof(Region)))
            {
                if (regionNumber >= firstRegionNumber && regionNumber <= (short)region)
                    return region;

                firstRegionNumber = (short)region;
            }

            return Region.Neizvesten;
        }

        private static short GetYear() => short.Parse(_egn.Substring(0, 2));

        private static byte GetMonth() => byte.Parse(_egn.Substring(2, 2));

        private static byte GetDay() => byte.Parse(_egn.Substring(4, 2));

        private static short GetRegionNumber() => short.Parse(_egn.Substring(6, 3));

        private static Gender GetGender() => (Gender)(byte.Parse(_egn.Substring(8, 1)) % 2);
    }
}
